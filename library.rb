require 'singleton'


#class to keep track of dates (singleton)
class Calendar
  include Singleton


  #to initialize single object call "Calendar.instance"
  def initialize
    @date = 0 #keeps track of the how many days have passed
  end


  #returns the current date
  def get_date
    @date.to_i
  end


  #increments the date and returns the new date
  def advance
    @date = @date + 1
  end
end



#class which represents books
class Book


  def initialize(id, title, author)
    @id = id
    @title = title
    @author = author  #only one author per book possible
    @due_date = nil   #book is not checked out by default (nil)
  end


  #returns the book id
  def get_id
    @id.to_i
  end


  #returns the book title
  def get_title
    @title.to_s
  end


  #returns the book author
  def get_author
    @author.to_s
  end


  #returns the due date
  def get_due_date
    @due_date.to_i
  end


  #sets the book due date
  def check_out(due_date)
    @due_date = due_date
  end


  #set due date back to default (nil)
  def check_in()
    @due_date = nil
  end


  #returns a string of the form "id: title, by author”.
  def to_s
    @id.to_s + ": " + @title.to_s + ", by " + @author.to_s
  end
end



class Member


  def initialize(name, library)
    @name = name
    @library = library
    @books = Array.new
  end


  #returns the name of the member
  def get_name
    @name.to_s
  end


  #adds this book object to the set of books checked out by this member if
  #less than 3 books are already checked out by this member
  def check_out (book)
    if @books.length < 3
      @books << book
    end
  end


  #removes this book object from the set of books checked out by this member
  def give_back (book)
    @books.delete(book)
  end


  #returns the set of books currently checked out by this member
  def get_books
    @books.to_a
  end


  #prints members name and notice
  def send_overdue_notice (notice)
    puts @name.to_s + ", " + notice.to_s
  end
end



class Library
  include Singleton


  #initializes a Library object which is closed. It reads in collection.txt to populate its @books variable and creates
  #an empty dictionary for its @members
  #(singleton)
  #to initialize single object call "Calendar.instance"
  def initialize()

    @books = Array.new
    @is_open = false
    @current_member = nil
    @members = Hash.new
    @@cal = Calendar.instance

    #variables required to populate @books
    data = Array.new
    tempTitle = Array.new
    tempAuthor = Array.new
    i = 1

    #reads lines from file and separates by commas before adding to array
    File.open("collection.txt","r") do |handle|
      handle.each_line do |line|
        data << line.split(",")
      end
    end

    data.flatten!

    tempTitle = data.values_at(* data.each_index.select {|i| i.even?})
    tempAuthor = data.values_at(* data.each_index.select {|i| i.odd?})

    tempTitle.zip(tempAuthor).each do |title, author|
      temp_book = Book.new(i, title.to_s, author.to_s)
      @books <<  temp_book
      i = i + 1
    end
  end


  #opens the Library and advances to the next day
  def open()
    begin
      if @is_open == true
        raise "The library is already open!"
      else
        @@cal.advance
        @is_open = true
        "Today is day " + @@cal.get_date.to_s
      end
    end
  end


  #issues a library card to the person with this name
  def issue_card(name_of_member)
    begin
      if @is_open == false
        raise "Library is not open"
      elsif @members.has_key?(name_of_member)
        name_of_member.to_s + " already has a library card"
      else
        #@members = {name_of_member => Member.new(name_of_member, self)}
        #@members.merge!(name_of_member: Member.new(name_of_member, self))
        @members[name_of_member] = Member.new(name_of_member, self)
        "Library card issued to " + name_of_member.to_s
      end
    end
  end


  #quits library for renovations
  def quit()
    "The library is now closed for renovations."
  end


  #specifies which member is about to be served
  def serve(name_of_member)
    if @is_open == false
      raise "Library is not open"
    elsif !(@members.has_key?(name_of_member))
      name_of_member + " does not have a library card."
    else
      @current_member = @members[name_of_member]
      "Now serving " + name_of_member
    end
  end


  #checks out the book to the member currently being served
  def check_out(*book_ids) # 1..n book_ids
    is_book_found = false   #has book been found?
    if @is_open == false
      raise "Library is not open"
    elsif @current_member == nil
      raise "No Member is currently being served."
    else
      book_ids.each { |id|
          #puts id
       @books.each { |b|
          if b.get_id == id
            is_book_found = true
            b.check_out(@@cal.get_date + 7)   #telling book its checked out
            @current_member.check_out(b)      #assign checked out book to current member
            @books.delete(b)                  #delete book from available books array
          end
       }
       if !is_book_found
         raise "The library does not have book " + id.to_s + "."
       end
      }
      book_ids.size.to_s + " books have been checked out to " + @current_member.get_name
    end
  end


  #renews the books for the member currently being served
  def renew(*book_ids) # 1..n book_ids
    is_book_found = false   #has book been found?
    if @is_open == false
      raise "Library is not open"
    elsif @current_member == nil
      raise "No Member is currently being served."
    else
      book_ids.each { |id|
        @current_member.instance_variable_get(:@books).each { |b|
          if b.get_id == id
            is_book_found = true  #book was found
            b.check_out(@@cal.get_date + 7)   #telling book its checked out
          end
        }
        if !is_book_found
          raise "The member does not have book " + id.to_s + "."
        end
      }
      book_ids.size.to_s + " books have been renewed for " + @current_member.get_name
    end
  end


  #finds available books in the library
  def search(string)
    is_book_found = false   #has book been found?
    is_book_duplicate = false   #more than one copy of a book?
    temp_string = ""
    tempBooks = Array.new
    if string.length < 4
      return "Search string must contain at least four characters."
    else
      @books.each { |b|
        if  b.get_title.downcase.include? string.downcase or b.get_author.downcase.include? string.downcase and b.get_due_date == 0
            is_book_found = true  #book was found

            tempBooks.each { |tb|
              if tb.get_author == b.get_author and tb.get_title == b.get_title
                is_book_duplicate = true
              end
            }
            if !is_book_duplicate
              tempBooks << b
              is_book_duplicate = false
            end
          end
      }
      tempBooks.each { |tb|
        temp_string = temp_string + tb.to_s
      }
        if !is_book_found
          return "No books found."
        else
          return temp_string
        end
    end
  end


  #this methods is being used to return books to the library
  def check_in(*book_numbers) # * = 1..n of book numbers
    is_book_found = false
    if @is_open == false
      raise "Library is not open"
    elsif @current_member == nil
      raise "No Member is currently being served."
    else
      book_numbers.each { |id|
        @current_member.get_books.each { |b|
          if b.get_id == id
            is_book_found = true
            b.check_out(0)
            @current_member.give_back(b)    #telling book its checked out
          end
        }
        if !is_book_found
          raise "The member does not have book " + id.to_s + "."
        end
      }
    end
        return @current_member.get_name + " has returned " + book_numbers.size.to_s + " books."
  end


  #find overdue books of the current member
  def find_overdue_books() # * = 1..n of book numbers
    is_book_found = false
    temp_string = ""
    if @is_open == false
      raise "Library is not open"
    elsif @current_member == nil
      raise "No Member is currently being served."
    else
        @current_member.get_books.each { |b|
          if b.get_due_date < @@cal.get_date
            is_book_found = true
            temp_string = temp_string + b.to_s
          end
        }
    end
    if !is_book_found
      return "None"
    else
      return temp_string
    end
  end


  #shut down operations and go home for the night.
  #none of the other operations (except quit) can be used when the library is closed.
  def close
    begin
      if @is_open == false
        raise "The library is not open."
      else
        @is_open = false
        "Good night."
      end
    end
  end


  #returns the names of
  #members who have overdue books, and for each such member, the books that are overdue.
  def find_all_overdue_books()
    over_due_book_found = false
    return_string = ""

    @members.each { |key, member|
      temp_books = ""
      member.get_books.each { |b|
        if b.get_due_date < @@cal.get_date
          over_due_book_found = true
          temp_books = temp_books + b.to_s
        end
      }
      if over_due_book_found
        return_string = return_string + "Name Of Member: " + key + "\nOverdue Books:  \n" + temp_books + "\n"
      else
        return_string = "No books are overdue."
      end
    }
    return return_string
    end
end
