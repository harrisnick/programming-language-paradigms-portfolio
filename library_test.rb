require_relative 'library'
require 'test/unit'

#each test method has to be run on their own. Will fail if ran as class


class Test_Calendar < Test::Unit::TestCase


  def test_advance
    assert_equal(1, Calendar.instance.advance)
  end

  def test_get_date
    assert_equal(1, Calendar.instance.get_date)
  end
end


class Test_Book < Test::Unit::TestCase

  # Called before every test method runs
  #def setup
  # b = Book.new(123, "Test Title", "Test Author")
  #end

  #also tests the initialize method
  def test_get_id
    b = Book.new(123, "Test Title", "Test Author")
    assert_equal(123, b.get_id)
  end

  #also tests the initialize method
  def test_get_title
    b = Book.new(123, "Test Title", "Test Author")
    assert_equal("Test Title", b.get_title)
  end

  #also tests the initialize method
  def test_get_author
    b = Book.new(123, "Test Title", "Test Author")
    assert_equal("Test Author", b.get_author)
  end

  #also tests the check_out method
  def test_get_due_date
    b = Book.new(123, "Test Title", "Test Author")
    b.check_out(12)
    assert_equal(12, b.get_due_date)
  end


  #also tests get_due_date method
  def test_check_out
    b = Book.new(123, "Test Title", "Test Author")
    b.check_out(10)
    assert_equal(10, b.get_due_date)
  end


  #also tests get_due_date method and check_out method
  def test_check_in
    b = Book.new(123, "Test Title", "Test Author")
    b.check_out(10)
    b.check_in
    assert_equal(0, b.get_due_date)
  end


  def test_to_s
    b = Book.new(123, "Test Title", "Test Author")
    assert_equal("123: Test Title, by Test Author", b.to_s)
  end

  # Called after every test method runs
  def teardown

  end

end



class Test_Member < Test::Unit::TestCase

  #also tests the initialize method
  def test_get_name
    m = Member.new("Test name", "Test Library")
    assert_equal("Test name", m.get_name)
  end


  #also tests the initialize method
  def test_check_out
    b = Book.new(123, "Test Title", "Test Author")
    m = Member.new("Test name", "Test Library")
    m.check_out(b)
    assert_equal(1,m.get_books.length)
    m.check_out(b)
    assert_equal(2,m.get_books.length)
    m.check_out(b)
    assert_equal(3,m.get_books.length)
  end


  #also tests the initialize method
  def test_check_out_more_than_3_books
    b = Book.new(123, "Test Title", "Test Author")
    m = Member.new("Test name", "Test Library")
    m.check_out(b)
    assert_equal(1,m.get_books.length)
    m.check_out(b)
    assert_equal(2,m.get_books.length)
    m.check_out(b)
    assert_equal(3,m.get_books.length)
    m.check_out(b)
    assert_equal(3,m.get_books.length) #expected value should still be 3 books
  end


  #also tests the initialize method and check_out method
  def test_give_back
    b = Book.new(123, "Test Title", "Test Author")
    m = Member.new("Test name", "Test Library")
    m.check_out(b)
    m.give_back(b)
    assert_equal(0,m.get_books.length)
  end


  #also tests the initialize method and check_out method
  def test_get_books
    b = Book.new(123, "Test Title", "Test Author")
    b2 = Book.new(321, "Native Son", "Wright")
    m = Member.new("Test name", "Test Library")
    m.check_out(b)
    m.check_out(b2)
    temp_array = Array.new
    temp_array << b
    temp_array << b2
    assert_equal(m.get_books, temp_array)
  end

end

class Test_Library < Test::Unit::TestCase

  def teardown
    lib = Library.instance
    lib.close
  end

  #first at the beginning as it has to run first
  def first_test_open
    assert_equal("Today is day 1", Library.instance.open)
  end


  def test_open_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.open
      lib.open
    end
    assert_equal("The library is already open!", exception.message) #test the exception message
  end


  def test_issue_card_library_not_open_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.issue_card("Test")
    end
    assert_equal("Library is not open", exception.message) #test the exception message
  end


  #also test if Member gets created and added to @member array when method called without errors
  def test_issue_card_member_already_has_card
    lib = Library.instance
    lib.open
    lib.issue_card("Test")
    message = lib.issue_card("Test")
    assert_equal("Test already has a library card", message)
  end


  def test_quit
    assert_equal("The library is now closed for renovations.", Library.instance.quit)
  end


  def test_serve_member_has_no_card
    lib = Library.instance
    lib.open
    message = lib.serve("Test Member")
    assert_equal("Test Member does not have a library card.", message)
  end


  def test_serve_library_not_open_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.serve("Test")
    end
    assert_equal("Library is not open", exception.message) #test the exception message
  end


  def test_serve
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    message = lib.serve("Test Member")
    temp_member = lib.instance_variable_get(:@current_member)
    assert_equal("Test Member", temp_member.get_name)
    assert_equal("Now serving Test Member", message)
  end


  def test_serve_return_message
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    message = lib.serve("Test Member")
    assert_equal("Now serving Test Member", message)
  end


  def test_check_out_library_not_open_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.check_out(123)
    end
    assert_equal("Library is not open", exception.message) #test the exception message
  end


  def test_check_out_no_member_being_served_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.open
      lib.check_out(123)
    end
    assert_equal("No Member is currently being served.", exception.message) #test the exception message
  end


  def test_check_out_book_not_in_library_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.open
      lib.issue_card("Test Member")
      lib.serve("Test Member")
      lib.check_out(123)
    end
    assert_equal("The library does not have book 123.", exception.message) #test the exception message
  end

  def test_check_out
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")
    lib.check_out(1)
    temp_member = lib.instance_variable_get(:@current_member)
    message = temp_member.instance_variable_get(:@books).to_s
    assert_equal("[1: 1984, by George Orwell\n]", message)
  end


  def test_renew_library_not_open_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.renew(1)
    end
    assert_equal("Library is not open", exception.message) #test the exception message
  end


  def test_renew_no_member_being_served_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.open
      lib.renew(1)
    end
    assert_equal("No Member is currently being served.", exception.message) #test the exception message
  end


  def test_renew_return_message
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")
    message = lib.check_out(1)
    assert_equal("1 books have been checked out to Test Member", message)
  end


  def test_renew
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")
    lib.check_out(2)
    lib.renew(2)
    temp_member = lib.instance_variable_get(:@current_member)
    temp_books = temp_member.instance_variable_get(:@books)
    temp_date = 0
    temp_books.each { |b| temp_date = b.get_due_date}
    assert_equal(8, temp_date)
  end


  def test_search_not_enough_chars
    lib = Library.instance
    message = lib.search("MLK")
    assert_equal("Search string must contain at least four characters.", message)
  end


  def test_search_book_not_found
    lib = Library.instance
    message = lib.search("MLK JR.")
    assert_equal("No books found.", message)
  end

  #collection.txt file that has been part of submission has to be used for this method to work
  def test_search_single_book
    lib = Library.instance
    message = lib.search("1984")
    assert_equal("1: 1984, by George Orwell\n", message)
  end


  #collection.txt file that has been part of submission has to be used for this method to work
  def test_search_multiple_books
    lib = Library.instance
    message = lib.search("flyn")
    assert_equal("20: Gone Girl, by Gillian Flynn\n21: Here Girl, by Gayle Flynson", message)
  end


  #collection.txt file that has been part of submission has to be used for this method to work
  def test_search_case_insensitivity
    lib = Library.instance
    message = lib.search("BrIeF")
    assert_equal("2: A Brief History of Time, by Stephen Hawking\n", message)
  end


  def test_check_in_library_not_open_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.check_in(1)
    end
    assert_equal("Library is not open", exception.message) #test the exception message
  end


  def test_check_in_no_member_being_served_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.open
      lib.check_in(1)
    end
    assert_equal("No Member is currently being served.", exception.message) #test the exception message
  end


  def test_check_in_single_book
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")

    temp_member = lib.instance_variable_get(:@current_member)
    temp_books = temp_member.instance_variable_get(:@books)

    lib.check_out(1)
    message = temp_books.length
    assert_equal(1, message)

    lib.check_in(1)
    message = temp_books.length
    assert_equal(0, message)
  end


  def test_check_in_multiple_books
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")

    temp_member = lib.instance_variable_get(:@current_member)
    temp_books = temp_member.instance_variable_get(:@books)

    lib.check_out(1, 2)
    message = temp_books.length
    assert_equal(2, message)

    lib.check_in(1, 2)
    message = temp_books.length
    assert_equal(0, message)
  end


  def test_check_in_book_not_found_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.open
      lib.issue_card("Test Member")
      lib.serve("Test Member")

      lib.check_in(1)
    end
    assert_equal("The member does not have book 1.", exception.message) #test the exception message
  end


  def test_check_in_return_message
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")
    lib.check_out(1)

    message = lib.check_in(1)
    assert_equal("Test Member has returned 1 books.", message)
  end


  def test_find_overdue_books_library_not_open_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.find_overdue_books()
    end
    assert_equal("Library is not open", exception.message) #test the exception message
  end


  def test_find_overdue_books_no_member_being_served_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.open
      lib.find_overdue_books()
    end
    assert_equal("No Member is currently being served.", exception.message) #test the exception message
  end


  def test_find_overdue_books
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")
    lib.check_out(1)
    lib.check_out(2)

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    message = lib.find_overdue_books
    assert_equal("1: 1984, by George Orwell\n2: A Brief History of Time, by Stephen Hawking\n", message)
  end


  def test_find_overdue_books_none
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")
    lib.check_out(1)

    message = lib.find_overdue_books
    assert_equal("None", message)
  end


  def test_close_library_not_open_exception
    exception = assert_raise(RuntimeError) do
      lib = Library.instance
      lib.close()
    end
    assert_equal("The library is not open.", exception.message) #test the exception message
  end


  def test_find_all_overdue_books
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")
    lib.check_out(1, 2)

    lib.issue_card("Test Member2")
    lib.serve("Test Member2")
    lib.check_out(3, 4)

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    lib.close
    lib.open

    message = lib.find_all_overdue_books

    assert_equal("Name Of Member: Test Member\nOverdue Books:  \n1: 1984, by George Orwell\n2: A Brief History of Time, by Stephen Hawking\n\nName Of Member: Test Member2\nOverdue Books:  \n3: A Heartbreaking Work of Staggering Genius,
by Dave Eggers\n4: A Long Way Gone, by Ishmael Beah\n\n", message)
  end


  def test_find_all_overdue_books_no_books
    lib = Library.instance
    lib.open
    lib.issue_card("Test Member")
    lib.serve("Test Member")
    lib.check_out(1, 2)

    lib.issue_card("Test Member2")
    lib.serve("Test Member2")
    lib.check_out(3, 4)

    message = lib.find_all_overdue_books

    assert_equal("No books are overdue.", message)
  end


end